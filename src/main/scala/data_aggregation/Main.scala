package data_aggregation
import java.io.{BufferedWriter, File, FileWriter}
import scala.util.control.Breaks.break
import com.google.gson.Gson
import scala.io.Source

object Main {
  def main(args: Array[String]): Unit = {
    val u_data : Iterator[String] = Source.fromResource("ml-100k/u.data").getLines()
    var data: List[(Int, Int)] = List()
    u_data.foreach(line => {
      val row = line.split('\t')
      if (row.length < 4) {
        break
      } else {
        data = data :+ ((row(1).toInt, row(2).toInt))
      }
    })

    val hist_film_map = data.filter(_._1.equals(82)).groupBy(_._2).mapValues{f => f.length}.toMap
    val hist_all_map = data.groupBy(_._2).mapValues{f => f.length}.toMap

    val hist_film = getResultList(hist_film_map)
    val hist_all = getResultList(hist_all_map)

    val json_result = JsonResult(hist_film = hist_film, hist_all = hist_all)
    val json_string = new Gson().toJson(json_result)
    val file = new File("result.json")
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(json_string)
    bw.close()
    println("Write to result.json done")
  }

  def getResultList(map: Map[Int, Int]): Array[Int] = {
    var res: Array[Int] = Array()
    val marks = Array(1, 2, 3, 4, 5)
    marks.foreach(mark => {
      val markCount = if (map.contains(mark)) map(mark) else 0
      res = res :+  markCount
    })
    res
  }
}
